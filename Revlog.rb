require 'digest'
require 'zlib'
load "compress.rb"

md5 = Digest::MD5.new
#md5.update path
s = md5.hexdigest
$nullid = s
$dummyLocalID = -1

module Mdiff
    def patch(oldT, newT)
        temp = ""
        temp << oldT
        return temp if newT == nil
        # p "newT: " , newT
        # p "temp: " , temp
        return temp << newT
    end

    def textdiff(oldT,newT)
        # p "oldT",oldT
        # p "newT",newT
        if oldT[0..-1] == newT[0...oldT.length]
            nnnn = newT[oldT.length..newT.length]
            # p "nnnn",nnnn
            return newT[oldT.length..newT.length]
        else
            len = (newT.split(".txt")[0]+".txt").length
            newnewT = newT[len..newT.length]
            return newnewT
        end

        
    end

    def linesplit(a)

        l = Array.new
        last = 0
        l << a[last..-1] if last < a.length
        return l
    end
end


class Revlog
    include Mdiff
    attr_accessor :indexfile
    attr_accessor :datafile
    attr_accessor :index
    attr_accessor :nodemap
    def initialize(indexfile,datafile)
        @indexfile = indexfile
        @datafile = datafile
        @index = Array.new(){|element| element} # use lambda function to make the stored object mutable
        @nodemap = Hash[$dummyLocalID =>$nullid, $nullid => $dummyLocalID ]
        begin
            n=0
            f = self.open(@indexfile) 
            # p "fffffffffff",f
            raise "file cannot be opened? " if f == nil
            fread = f.readlines()

            fread.each do |fline|
                    fline2 = decompress_decode(fline) 
                    e = fline2.split(" ")
                    e = e[0..5]
                    if not e.empty?
                        for i in 0..4
                            e[i]=e[i].to_i
                        end
                        @nodemap[e[5]] = n
                        @index << e
                        n = n+1
                    end
            end
            f.close

        rescue 
        end
    end

    def index()
        return @index
    end

    def nodemap()
        return @nodemap
    end
    def open(fn, mode='r')
        return File.open(fn,mode)
    end

    def tip()
        return @index.length - 1
    end

    def node(rev)
        if rev < 0 
            return $nullid
        else
            return @index[rev][5]
        end
    end

    def rev(node)
        return @nodemap[node]
    end

    def parents(rev)
        return @index[rev][3..4]
    end

    def start(rev)
        return @index[rev][0]
    end  

    def length(rev)
        return @index[rev][1]
    end

    def end(rev)
        return self.start(rev) + self.length(rev)
    end

    def base(rev)
        return @index[rev][2]
    end

    def revision(rev)
        if rev == -1
            return ""
        end
        base = self.base(rev)

        start = self.start(base)
        endPoint = self.end(rev)

        f = self.open(@datafile)
        f.seek(start)
        data = f.read(endPoint - start)
        last = self.length(base)
        textTemp = data[0...last-1]
        text = textTemp

        (base+1..rev).each do |r|
            s = self.length(r)
            textTemp = data[last...last+s-1]
            b = textTemp
            text = patch(text,b)
            last = last + s
        end

        p = self.parents(rev)
        p1 = p[0]
        p2 = p[1]
        n1 = ""    
        n2 = ""
        if p1 != 0 
            n1 = self.node(p1)
        end
        if p2 != 0
            n2 = self.node(p2)
        end
        
        temp = n1 + n2 + text
        node = temp
        if self.node(rev) !=node
            raise "Consistency check failed #{__method__}"
        end
        return text
    end

    def revision2(rev)
        if rev == -1
            return ""
        end
        base = self.base(rev)
        start = self.start(base)
        endPoint = self.end(rev)

        f = self.open(@datafile)
        f.seek(start)
        if f.read(1) == '\n'
            f.seek(start+1)
        end
        data = f.read(endPoint - start)
        last = self.length(base)
        textTemp = data[0...last-1]
        text = textTemp

        (base+1..rev).each do |r|
            s = self.length(r)
            textTemp = data[last...last+s-1]
            b = textTemp
            text = patch(text,b)
            last = last + s
        end

        p = self.parents(rev)
        p1 = p[0]
        p2 = p[1]
        n1 = ""    
        n2 = ""
        if p1 != 0 
            n1 = self.node(p1)
        end
        if p2 != 0
            n2 = self.node(p2)
        end
        temp = n1 + n2 + text
        node = temp
        if self.node(rev) !=node
            raise "Consistency check failed #{__method__}"
        end
        return text
    end

    def addrevision(text, p1=nil, p2=nil)
        if text==nil
            text=""
        end


        if p1==nil
            p1=self.tip()
        end

        if p2==nil
            p2 = -1
        end

        t = self.tip()
        n = t + 1

        if n>0
            start = self.start(self.base(t))
            endPoint = self.end(t)
            prev = self.revision(t)
            med = textdiff(prev,text)
            data = med 
        end
        data = "" if data == nil
        if n<=0 or (endPoint+data.length-start) > (2*text.length)
            data = text
            base = n
        else
            base = self.base(t)
        end

        offset = 0
        if t>= 0
            offset = self.end(t)
        end

        n1=self.node(p1)
        n2=self.node(p2)
        n1 = "" if n1 == nil
        n2 = "" if n2 == nil
        text = "" if text == nil
        temp =n1+n2+text
        node = temp
        e = [offset,data.length,base,p1,p2,node]
        @index << e
        self.nodemap[node]=n
        entry = ""
        e.each do |c|
            entry << c.to_s
            entry << " "
        end

        entry = compress_encode(entry)
        self.open(@indexfile,'a').write(entry)
        self.open(@datafile,'a').write(data)
        return n
    end


    def ancestor(a,b)
        #  [old_tip],[new_tip],{old:1},{new:1}
        def expand(e1,e2,a1,a2)
            ne = Array.new(){|element| element} 
            e1.each do |r|
                p = self.parents(r)
                p1 = p[0]
                p2 = p[1]
                return p1 if a2.has_key?(p1)
                return p2 if a2.has_key?(p2)
                if !a1.has_key?(p1)
                    a1[p1]=1
                    ne << p1
                    if p2 >= 0 && !a1.has_key?(p2)
                        a1[p2]=1
                        ne << p2
                    end
                end
            end
            return expand(e2,ne,a2,a1)
        end
        return expand([a],[b],{a => 1},{b => 1})
    end

    def revisions(list)
        # only used by mergedag
        text = []
        list.each do |r|
            text << self.revision(r)
        end
        # p "revisionsssss text",text
        # p "class of text",text.class()
        # p "class of text.each",text.each
        return text.each
    end


    def mergedag(other, accumulate=nil)
        amap = @nodemap
        # p "amap",amap
        # p "self",self
        bmap = other.nodemap
        i = self.tip()
        # p "old tip",i
        old = i
        #store: [tip, rev(p1), rev(p2)]
        lst = Array.new(){|element| element} 
        #store: 0..old_tip
        rList = Array.new(){|element| element} 

        (0..other.tip()).each do |r|
            id = other.node(r)
            if !amap.has_key?(id)
                i = i+1
                parents_xy = other.parents(r)
                x = parents_xy[0]
                # p "x",x
                y = parents_xy[1]
                xn = other.node(x)
                yn = other.node(y)
                # p "xn",xn
                lst << [r, amap[xn], amap[yn]]
                rList << r
                # node id => new tip
                amap[id]=i
            end
        end

        r = other.revisions(rList)
        lst.each do |e|
            totalText = r.next()
            # p "totalText",totalText
            accumulate.call(totalText) if accumulate
            self.addrevision(totalText, e[1], e[2])
        end

        return [old, self.tip()] # old_tip, new_tip

    end


    def resolvedag(oldNode,newNode)
        if oldNode==newNode
            return nil 
        end
        a = self.ancestor(oldNode,newNode)
        if oldNode == a 
            return newNode 
        end
        # if old_tip has nothing to do with new_tip: do merge3()
        return self.merge3(oldNode,newNode,a)
    end


    def merge(other)
        tempArray = self.mergedag(other)
        return self.resolvedag(tempArray[0], tempArray[1])
    end

    # old_tip has nothing to do with new_tip
    def merge3(my, other, base)
        t = self.revision(my)
        return self.addrevision(t, my, other)
    end


end

class Filelog < Revlog # store name and id
    def initialize(repo, path)
        @repo = repo
        md5 = Digest::MD5.new
        md5.update path
        s = md5.hexdigest
        super(File.join("index/",s), File.join("data/",s))
    end

    def open(file, mode = "r")
        return @repo.open(file, mode)
    end
end



class Manifest < Revlog # contains file name and node id
    include Mdiff
    def initialize(repo)
        @repo = repo
        super("00manifest.i", "00manifest.d")           
    end

    def open(file, mode="r") 
        return @repo.open(file, mode)
    end

    def manifest(rev)
        text = self.revision(rev)
        
        map = {}
        if linesplit(text) != [""]
            for l in linesplit(text) do
                ii = l.index("@")
                map[l[(ii+1)..-1]] = l[0...ii]
            end
        end
        return map
    end

    def addmanifest(map, p1=nil, p2=nil)
        map.delete(nil) if map[nil] != nil
        files = map.keys
        files.sort 
        text = ""
        lll = Array.new
        for f in files do
            text = text + (map[f] + "@" + f + "\n")
        end
        return self.addrevision(text, p1, p2)
    end
end

class Changelog < Revlog
    include Mdiff
    include Enumerable
    def initialize(repo)
        @repo = repo
        super("00changelog.i", "00changelog.d")
    end

    def open(file, mode="r")
        return @repo.open(file, mode)
    end

    def extract(text)
        l = text.split(" ")
        l.delete("")
        manifest = l[0] # all content of the first line of text
        user = l[1] # all content of the second line of text
        date = l[2] # all content of the third line of text 
        files = Array.new
        files << l[3]
        desc = ""
        desc = l[-1]
        res = Array.new
        res << manifest
        res << user
        res << date
        res << files
        res << desc
        return res
    end

    def extract2(text)
        l = text.split(" ")
        l.delete("")
        manifest = l[-5] # all content of the first line of text
        user = l[-4] # all content of the second line of text
        date = l[-3] # all content of the third line of text
        # last = l.index("\n") # find the index of first '\n'  
        files = Array.new

        files << l[-2]
        # files = l[3]
        desc = ""
        desc = l[-1]
        res = Array.new
        res << manifest
        res << user
        res << date
        res << files
        res << desc
        return res
    end

    def changeset(rev)
        return self.extract(self.revision(rev))
    end

    def changeset2(rev)
        return self.extract2(self.revision2(rev))
    end

    def addchangeset(manifest, list, desc, p1=nil, p2=nil)
        time1 = Time.new
        user = "User_name"
        date = time1.inspect
        date = date[0...-6]
        date.sub!(" ", ";")
        list = list.sort
        manifest.sub!("\n","")
        l = [manifest, user, date] + list + ["", desc]
        text = ""
        for e in l
            text += (e.to_s + " ")  
        end
        return self.addrevision(text, p1, p2)
    end

end