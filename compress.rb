require "zlib"
require "base64"

def compress_encode(str)
    compress_str = Zlib::Deflate.deflate(str)
    encoded_str = Base64.encode64 compress_str
    encoded_str.tr!("\n", "*")
    encoded_str = encoded_str.chomp("*")
    encoded_str = encoded_str + "\n"
    return encoded_str
end

def decompress_decode(encoded_data)
    encoded_data.tr!("*", "\n")
    base = Base64.decode64(encoded_data)
    str2 = Zlib::Inflate.inflate(base)
    return str2
end